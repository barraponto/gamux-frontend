This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Resources

[Hamburger Menu][noun-menu] by Stan Diers from the Noun Project.

[noun-menu]: https://thenounproject.com/term/hamburger-menu/204478/
