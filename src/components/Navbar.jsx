import React from "react";
import styled from "styled-components";
import menu from "./menu.svg";

const Bar = styled.header`
  display: flex;
  padding: 1rem;
  background-color: black;
  color: white;
`;

const Brand = styled.h1`
  margin: 0;
  flex-grow: 1;
  font-size: 1.4rem;
  line-height: 2rem;
`;

const MenuIcon = styled.img`
  padding: 0.2rem 0;
  height: 1.6rem;
`;

export default () => (
  <Bar className="app-header">
    <Brand className="app-title">Gamux</Brand>
    <MenuIcon src={menu} alt="Menu" />
  </Bar>
);
