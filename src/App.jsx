import React from "react";
import Navbar from "./components/Navbar";
import Intro from "./components/Intro";

const App = () => (
  <React.Fragment>
    <Navbar />
    <Intro />
  </React.Fragment>
);

export default App;
